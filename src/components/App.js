import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import logo from "../assets/bagel.svg";
import BagelDisplayCard from "./BagelDisplayCard";
import BigRedOrderButton from "./BigRedOrderButton";
import ShoppingCart from "./ShoppingCart";
import ArrowDown from "./ArrowDown";

import find from "lodash/find";

import { bagelList } from "../assets";

const emptyOrder = {};
bagelList.forEach(b => {
  emptyOrder[b.bagelTitle] = 0;
});

const calculatePrice = order => {
  let price = 0;
  Object.keys(order).forEach(b => {
    price =
      price + order[b] * find(bagelList, o => b === o.bagelTitle).bagelPrice;
  });
  return +price.toFixed(2);
};
class App extends Component {
  state = {
    order: emptyOrder,
    price: 0
  };

  addBagel = type => {
    let newOrder = { ...this.state.order, [type]: this.state.order[type] + 1 };
    let newPrice = calculatePrice(newOrder);
    this.setState({ order: newOrder, price: newPrice });
  };

  render() {
    return (
      <div className="content-wrapper">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload again, and reload again.
          </p>
          <ArrowDown />
        </header>
        <div className="bagel-second-header">
          <h2>
            <FontAwesomeIcon icon={faStar} color="#DC3545" />
            &nbsp;
            <FontAwesomeIcon icon={faStar} color="#DC3545" />
            &nbsp;
            <span className="highlight">Order Cold War Bagels Below</span>
            &nbsp;
            <FontAwesomeIcon icon={faStar} color="#DC3545" />
            &nbsp;
            <FontAwesomeIcon icon={faStar} color="#DC3545" />
          </h2>
        </div>
        <div className="content-wrapper">
          <div className="container">
            <div className="row">
              {bagelList.map(b => {
                console.log(b);
                return (
                  <BagelDisplayCard
                    key={b.bagelTitle}
                    bagel={b}
                    addBagel={this.addBagel}
                  />
                );
              })}
            </div>
            <ShoppingCart order={this.state.order} price={this.state.price} />
            <BigRedOrderButton order={this.state.order} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
