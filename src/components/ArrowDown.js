import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";

export default function ArrowDown() {
  return (
    <div className="position-bottom-of-page">
      <FontAwesomeIcon icon={faChevronDown} size="3x" color="#DC3545" />
    </div>
  );
}
