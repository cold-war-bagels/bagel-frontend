import React from "react";
import defaultBagel from "../assets/bagel01.jpg";

const BagelDisplayCard = props => {
  let { bagelPrice, bagelTitle, bagelDesc, bagelImage } = props.bagel;
  return (
	  <div className="col-lg-4">

    <div className="bagel-card">
      <div className="bagel-image-container">
        <img
          className="bagel-image"
          src={bagelImage ? bagelImage : defaultBagel}
          alt="bagel"
        />
      </div>
      <div className="bagel-text">
        <h6 className="bagel-text-price">
          {bagelPrice ? `${bagelPrice}/Bagel` : "$3/Bagel"}
        </h6>
        <b className="bagel-text-title">
          <span className="highlight">
            {bagelTitle ? bagelTitle : "The Iron Curtain"}
          </span>
        </b>
        <p className="bagel-text-description">
          {bagelDesc
            ? bagelDesc
            : "Did you remember to get your iron? This bagel is a reminder. Raisins, cinnamon and enriched flour leave nothing to be desired."}
        </p>
      </div>
      <div className="bagel-add-to-cart">
        <button
          onClick={() => props.addBagel(bagelTitle)}
          className="btn btn-danger btn-block text-light rounded-0"
        >
          Add to cart
        </button>
      </div>
    </div>
	  </div>
  );
};

export default BagelDisplayCard;
