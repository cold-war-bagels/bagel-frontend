import React from "react";
import axios from "axios";

// const slackIntegrationURL =
//   "https://hooks.slack.com/services/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";

// const googleCloudFunctionURL =
//   "https://us-central1-cold-war-bagels.cloudfunctions.net/function-1";

const localDevServer = "http://localhost:3000";

// const exampleOrder = {
//   customer: {
//     name: "Joe Doe",
//     address: "Bagel Court, Nevada City, CA"
//   },
//   order: [
//     {
//       bagel: "Iron Curtain",
//       amount: 2
//     },
//     {
//       bagel: "Cuba Libre",
//       amount: 1
//     }
//   ],
//   price: 12.95
// };

const order = props => {
  console.log("props", props);
  const config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
      "Access-Control-Allow-Origin": "*"
    }
  };
  // const payload = {
  //   fallback:
  //   "Required text summary of the attachment that is shown by clients that understand attachments but choose not to show them.",

  // text: "NEW BAGEL ORDER!",
  // pretext: `ordered at ${new Date()}`,
  // color: "#36a64f", // Can either be one of 'good', 'warning', 'danger', or any hex color code
  // // Fields are displayed in a table on the message
  // fields: [
  //   {
  //     title: `Customer: ${Object.keys(exampleOrder.customer).map(
  //       k => exampleOrder.customer[k]
  //     )}`, // The title may not contain markup and will be escaped for you
  //     value: `order: ${exampleOrder.order.map(
  //       m => `bagel : ${m.bagel}, amount: ${m.amount} \n `
  //     )} \n price: ${exampleOrder.price}`,
  //     short: false // Optional flag indicating whether the `value` is short enough to be displayed side-by-side with other values
  //   }
  // ]
  // customer: exampleOrder.customer.name
  // customer: "DDDDDD",
  // order: props.order
  // };

  let data = {
    userId: "1",
    title: "todoTitle",
    completed: false
  };

  axios
    .post(localDevServer, data, config)
    .then(() => {
      console.log("Order placed");
    })
    .catch(() => {
      console.log("There was a problem ordering");
    });
};

export default function BigRedOrderButton(props) {
  return (
    <div className="bagel-button-container">
      <div className="order-button" onClick={() => order(props)} />
    </div>
  );
}
