import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { bagelList } from "../assets";

import find from "lodash/find";

export default function ShoppingCart(props) {
  return props.price > 0 ? (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th scope="col"><FontAwesomeIcon icon={faStar} color="#DC3545" />
          &nbsp;Bagel&nbsp;<FontAwesomeIcon icon={faStar} color="#DC3545" /></th>
            <th scope="col">Amount</th>
            <th scope="col">Price</th>
          </tr>
        </thead>
        <tbody>
          {Object.keys(props.order).map(o => {
            return props.order[o] ? (
              <tr key={o}>
                <th scope="row">{o}</th>
                <td>{props.order[o]}</td>
                <td>
                  {find(bagelList, el => el.bagelTitle === o).bagelPrice *
                    props.order[o]}
                </td>
              </tr>
            ) : null;
          })}
        </tbody>
      </table>
      <h4>Total Price: {props.price}$</h4>
    </div>
  ) : null;
}
